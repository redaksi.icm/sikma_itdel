<?php

namespace backend\modules\kmhs;

class Kmhs extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\kmhs\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
