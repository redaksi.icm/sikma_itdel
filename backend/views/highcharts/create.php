<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Highcharts */

$this->title = 'Create Highcharts';
$this->params['breadcrumbs'][] = ['label' => 'Highcharts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="highcharts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
