<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BeasiswaCompany */

$this->title = Yii::t('app', 'Create Beasiswa Company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Beasiswa Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beasiswa-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
