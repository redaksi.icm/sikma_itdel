<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BeasiswaCompany */

$this->title = Yii::t('app', 'Update Beasiswa Company: {name}', [
    'name' => $model->id_company,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Beasiswa Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_company, 'url' => ['view', 'id' => $model->id_company]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="beasiswa-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
