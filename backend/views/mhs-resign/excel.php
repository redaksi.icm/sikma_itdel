<?php
	\backend\assets\web\php\ExcelGrid::Widget([
		'dataProvider'=> $dataProvider,
		'filterModel' => $searchModel,
		'filename'	  => 'Mahasiswa Resign',
		'properties'  => [

		],
		'columns'	 => [
			['class' =>  'yii\grid\SerialColumn'],
            'nama_mhs',
            'nim',
            'prodi',
            'alasan',

		]
	])
?>