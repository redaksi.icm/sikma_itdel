<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\KegiatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dokumentasi Prestasi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestasi-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p> <?= Html::a('Export ke Excel', ['excel'], ['class' => 'btn btn-primary']) ?> 
</p>
<<<<<<< backend/views/prestasi/index.php
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    
    'columns' => [
       
        'jenis_prestasi',
        'tgl_awal_kegiatan',
        'tgl_akhir_kegiatan',
        
=======

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'jenis_prestasi',
            'tgl_awal_kegiatan',
            'tgl_akhir_kegiatan',
>>>>>>> backend/views/prestasi/index.php
            // 'keterangan',
        //     ['attribute' => 'upload_file',
        //     'label' => 'File Prestasi',
        //     'format' => 'html',
        //     'value' => 
        //     function($model){
        //         return Html::a($model->upload_file, ['prestasi/download', 'id'=> $model->id_prestasi]);
        //     }
        // ],
        ['attribute' => 'status_request_id',
        'label' => 'Status Request',
        'value' => function($model){
            if(is_null($model->statusRequest['name'])){
                return '-';
            }else{
                return $model->statusRequest['name'];
            }
        }
    ],

    ['class' => 'common\components\ToolsColumn',
    'template' => '{view} {approve} {reject}',
    'header' => 'Aksi',
    'buttons' => [
        'view' => function ($url, $model){
            return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
        },
        'reject' => function ($url, $model){
            if ($model->status_request_id == 1 || $model->status_request_id == 2 ) {
                return "";
            }else{
                return ToolsColumn::renderCustomButton($url, $model, 'Reject', 'fa fa-times');
            }
        },
        'approve' => function ($url, $model){
            if ($model->status_request_id == 1 || $model->status_request_id == 2 ) {
                return "";
            }else{
                return ToolsColumn::renderCustomButton($url, $model, 'Approve', 'fa fa-check');
            }
        },

        'urlCreator' => function ($action, $model, $key, $index){
            if ($action === 'view') {
              return Url::toRoute(['view', 'id' => $key]);
          }else if ($action === 'edit') {
              return Url::toRoute(['update', 'id' => $key]);
          }

      }


  ],
],
],

]); ?>

</div>
