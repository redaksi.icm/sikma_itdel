<?php
use backend\models\NewUser;

\backend\assets\web\php\ExcelGrid::Widget([
	'dataProvider'=> $dataProvider,
	'filterModel' => $searchModel,
	'filename'	  => 'Kompetisi',
	'properties'  => [

	],
	'columns'	 => [
		['class' =>  'yii\grid\SerialColumn'],
		['attribute' => 'firstname',
		'label' => 'Nama Mahasiswa',
		'value' => function ($data) {
			return NewUser::findOne(['id_mhs'=>$data->id_mhs])->username;
		},
	],
	['attribute' => 'jenis_kompetisi',
	'label' => 'Jenis Kompetisi'
],

['attribute' => 'upload_proposal',
'label' => 'File Proposal'],

['attribute' => 'status_request_id',
'label' => 'Status Request',
'value' => function($model){
	if(is_null($model->statusRequest['name'])){
		return '-';
	}else{
		return $model->statusRequest['name'];
	}
}
],
]
])
?>