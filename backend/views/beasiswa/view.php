<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Beasiswa */

$this->title = 'Data Beasiswa';
?>
<div class="beasiswa-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
     'model' => $model,
     'attributes' => [
        'nama_mhs',
        'alamat',
        'nim_mhs',
        'jlh_bersaudara',
        'anak_ke',
        'tanggungan',
        'pekerjaan_ayah',
        'pekerjaan_ibu',
        'penghasilan_ayah',
        'penghasilan_ibu',
        ['label' => 'Status Request', 'value' => $model->statusRequest->name],
        
    ]
    
]) ?>

</div>