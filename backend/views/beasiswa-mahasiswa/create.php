<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BeasiswaMahasiswa */

$this->title = Yii::t('app', 'Tambah Informasi Beasiswa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Beasiswa Mahasiswa'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beasiswa-mahasiswa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'beasiswaLainnyaModel' => $beasiswaLainnyaModel,
    ]) ?>

</div>
