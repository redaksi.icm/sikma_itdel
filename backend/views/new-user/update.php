<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NewUser */

$this->title = 'Edit Data Mahasiswa: ';
$this->params['breadcrumbs'][] = ['label' => 'New Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_mhs, 'url' => ['view', 'id' => $model->id_mhs]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="new-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
