<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
use backend\models\NewUser;

/* @var $this yii\web\View */
/* @var $model backend\modules\kmhs\models\KegiatanMahasiswa */
$this->title = 'Data LPJ Kegiatan';
?>
<div class="lpj-kegiatan-view">
  <h1><?= Html::encode($this->title) ?></h1>
  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [

     ['label'=>'Nama Mahasiswa',
     'value' => function ($data) {
      return NewUser::findOne(['id_mhs'=>$data->id_mhs])->username;
    },
  ],
  'jenis_kegiatan',
  ['attribute' => 'file_lpj',
  'label' => 'LPJ Kegiatan',
  'format' => 'html',
  'value' => 
  function($model){
    return Html::a($model->file_lpj, ['lpj-kegiatan/download', 'id'=> $model->id_LpjKegiatan]);
  }
],
['label' => 'Status Request', 'value' => $model->statusRequest->name],
],
]) ?>
  <?= Html::a('Lihat Pdf', [
    'pdf',
    'id' => $model->id_LpjKegiatan,
  ], [
    'class' => 'btn btn-primary',
    'target' => '_blank',
  ]); ?>
</div>