<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
  </div>
  <!--   <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div> -->
    <!--  </strong> Sistem Informasi Kemahasiswaan Copyright © D3TI08. All rights reserved. -->
    <strong>Kemahasiswaan Information System Copyright © D3TI08.</strong>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    
</aside> 
<div class='control-sidebar-bg'></div>
