<aside class="main-sidebar">

    <section class="sidebar">


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Prestasi',
                    'icon' => 'star', 
                    'url' => ['/'],
                    'items' => [
                        ['label' => 'Dokumentasi Prestasi',
                        'icon' => 'credit-card', 
                        'url' => ['/prestasi']],

                        ['label' => 'Grafik Prestasi',
                        'icon' => 'bar-chart', 
                        'url' => ['/highcharts']],

                    ],

                ],

                [   'label' => 'Kompetisi',
                'icon'  => 'trophy', 
                'url'   => ['/'],
                'items' => [
                   ['label' => 'Request Kompetisi',
                   'icon' => 'credit-card', 
                   'url' => ['/kompetisi']],

                   ['label' => 'LPJ Kompetisi',
                   'icon' => 'file-text', 
                   'url' => ['/lpj-kompetisi']],
               ],
           ],

           ['label' => 'Kegiatan',
           'icon' => 'calendar-o', 
           'url' => ['/'],
           'items' => [
            ['label' => 'Request Kegiatan',
            'icon' => 'credit-card', 
            'url' => ['/kegiatan']],

            ['label' => 'LPJ Kegiatan',
            'icon' => 'file-text', 
            'url' => ['/lpj-kegiatan']],

            [
                'label' => 'Request Ruangan',
                'icon' => 'building-o',
                'url' => ['/ruangan']],
            ],

            [
                'label' => 'Request Ruangan',
                'icon' => 'chevron-circle-down',
                'url' => ['/ruangan']],
            ],



            ['label' => 'Beasiswa',
            'icon' => 'th-list', 
            'url' => ['/'],
            'items' => [
                ['label' => 'Request Beasiswa',
                'icon' => 'credit-card', 
                'url' => ['/beasiswa']],

                ['label' => 'Informasi Beasiswa',
                'icon' => 'angle-double-right', 
                'url' => ['/beasiswa-mahasiswa']],

            ],

        ],
        [   'label' => 'Mahasiswa Resign',
        'icon'  => '    fa fa-edit', 
        'url'   => ['/mhs-resign'],
    ],

    [   'label' => 'Add Users',
    'icon'  => 'user', 
    'url'   => ['/new-user'],
],

],
]) 
?>

</section>

</aside>