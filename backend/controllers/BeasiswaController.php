<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Beasiswa;
use backend\models\BeasiswaSearch;
use backend\models\Notifikasi;


class BeasiswaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules'=> [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new BeasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 5];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprove($id)
    {
        $model = Beasiswa::find()->where(['id_beasiswa' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "Request Beasiswa Diterima";
        $model->status_request_id=1;
        
        $searchModel = new BeasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save(false)){
            $notif->save(false);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            
        }
    }

    public function actionReject($id)
    {
        $model = Beasiswa::find()->where(['id_beasiswa' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "Request Beasiswa Ditolak";
        $model->status_request_id=2;
        $searchModel = new BeasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save(false)){
    //disini buat savenya
           if ($notif->save(false)) {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        
    }
}

public function actionView($id)
{
    return $this->render('view', [
        'model' => $this->findModel($id),
    ]);
}


public function actionCreate()
{
    $model=new Beasiswa;
    if(isset($_POST['NamaModel']))
    {
        $model->attributes=$_POST['Beasiswa'];
        $model->status_request_id=0;
        if($model->save())
            $this->redirect(array('view','id'=>$model->id));
    }
    $this->render('create',array(
        'model'=>$model,
    ));
}


public function actionUpdates($id)
{
    $model=$this->loadModel($id);
    $model->status=1;
    if($model->save())
        $this->redirect(array('view','id'=>$model->id));

    $this->render('update',array(
        'model'=>$model,
    ));
}

public function actionDelete($id)
{
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
}


protected function findModel($id)
{
    if (($model = Beasiswa::findOne($id)) !== null) {
        return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
}

public function actionExcel()
{
    $searchModel = new BeasiswaSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('excel', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
}



public function actionLogin()
{
    if (!Yii::$app->user->isGuest) {
        return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
        return $this->goBack();
    } else {
        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }
}
}
