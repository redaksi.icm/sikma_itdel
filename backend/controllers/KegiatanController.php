<?php

namespace backend\controllers;

use Yii;
use backend\models\Kegiatan;
use backend\models\KegiatanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use yii\filters\AccessControl;
use backend\models\Notifikasi;

/**
 * KegiatanController implements the CRUD actions for Kegiatan model.
 */
class KegiatanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules'=> [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Kegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KegiatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 5];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprove($id)
    {
        $model = Kegiatan::find()->where(['id_kegiatan' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "Request Kegiatan Diterima";
        $model->status_request_id=1;
        $searchModel = new KegiatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save()){
            
           if ($notif->save(false)) {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        
    }
}

public function actionDownload($id) 
{ 
    $download = Kegiatan::findOne($id); 
    $path=Yii::getAlias('@frontend') . '/web/Kegiatan Mahasiswa/' . $download->upload_proposal;
    if (file_exists($path)) {
        return Yii::$app->response->sendFile($path);
    }
}

public function actionExcel()
{
    $searchModel = new KegiatanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('excel', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
}


public function actionReject($id)
{
    $model = Kegiatan::find()->where(['id_kegiatan' => $id])->one();
    $notif = new Notifikasi();
    $notif->id_mhs = $model->id_mhs;
    $notif->desc = "Request Kegiatan Ditolak";
    $model->status_request_id=2;
    $searchModel = new KegiatanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    if($model->save()){
    //disini buat savenya
       if ($notif->save(false)) {
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
}
}


    public function actionPdf($id) {
    $model = Kegiatan::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/Kegiatan Mahasiswa/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_proposal);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}

    /**
     * Displays a single Kegiatan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model=new Kegiatan;
        if(isset($_POST['NamaModel']))
        {
            $model->attributes=$_POST['Kegiatan'];
            $model->status_request_id=0;
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates an existing Kegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdates($id)
    {
        $model=$this->loadModel($id);
        $model->status=1;
        if($model->save())
            $this->redirect(array('view','id'=>$model->id));

        $this->render('update',array(
            'model'=>$model,
        ));
    }


    public function actionPdf($id) {
        $model = Kegiatan::findOne($id);

    // This will need to be the path relative to the root of your app.
        $filePath = '/web/Kegiatan Mahasiswa /';
    // Might need to change '@app' for another alias
        $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_proposal);

        
        return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);


    }


    /**
     * Deletes an existing Kegiatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    

    /**
     * Finds the Kegiatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kegiatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kegiatan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
}
