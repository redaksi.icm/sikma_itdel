<?php
namespace backend\controllers;

use Yii;
use backend\models\Prestasi;
use backend\models\PrestasiSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Notifikasi;



class PrestasiController extends Controller{

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules'=> [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){
        $searchModel = new PrestasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 5];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' =>$dataProvider,
        ]);
    }

    public function actionApprove($id)
    {
        $model = Prestasi::find()->where(['id_prestasi' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "Dokumentasi Prestasi Diterima";
        $model->status_request_id=1;
        $searchModel = new PrestasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save() && $notif->save(false)){

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            
        }
    }

    public function actionPdf($id) {
    $model = Prestasi::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/Prestasi Mahasiswa/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_file);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}


    public function actionReject($id)
    {
        $model = Prestasi::find()->where(['id_prestasi' => $id])->one();
        $notif = new Notifikasi();
        $notif->id_mhs = $model->id_mhs;
        $notif->desc = "Dokumentasi Prestasi Ditolak";
        $model->status_request_id=2;
        $searchModel = new LpjKegiatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->save()){
    //disini buat savenya
         if ($notif->save(false)) {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        
    }

public function actionCreate()
{
    $model=new Prestasi;
    if(isset($_POST['NamaModel']))
    {
        $model->attributes=$_POST['Prestasi'];
        $model->status_request_id=0;
        if($model->save())
            $this->redirect(array('view','id'=>$model->id));
    }
    $this->render('create',array(
        'model'=>$model,
    ));
}

public function actionDownload($id) 
{ 
    $download = Prestasi::findOne($id); 
    $path=Yii::getAlias('@frontend') . '/web/Prestasi Mahasiswa/' . $download->upload_file;
    if (file_exists($path)) {
        return Yii::$app->response->sendFile($path);
    }
}
public function actionUpdates($id)
{
    $model=$this->loadModel($id);
    $model->status=1;
    if($model->save())
        $this->redirect(array('view','id'=>$model->id));

    $this->render('update',array(
        'model'=>$model,
    ));
}

public function actionPdf($id) {
    $model = Prestasi::findOne($id);

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/Prestasi Mahasiswa /';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->upload_file);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);


}

protected function findModel($id)
{
    if (($model = Prestasi::findOne($id)) !== null) {
        return $model;
    } else {
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

public function actionView($id)
{
    return $this->render('view', [
        'model' => $this->findModel($id),
    ]);
}

public function actionExcel()
{
    $searchModel = new PrestasiSearch();
    $dataProvider = $searchModel->searchExcel(Yii::$app->request->queryParams);
    return $this->render('excel', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
}
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

