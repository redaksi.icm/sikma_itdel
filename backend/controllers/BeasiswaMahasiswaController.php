<?php

namespace backend\controllers;

use Yii;
use backend\models\BeasiswaMahasiswa;
use backend\models\BeasiswaMahasiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\DynamicModel;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * BeasiswaMahasiswaController implements the CRUD actions for BeasiswaMahasiswa model.
 */
class BeasiswaMahasiswaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BeasiswaMahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BeasiswaMahasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BeasiswaMahasiswa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BeasiswaMahasiswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BeasiswaMahasiswa();
        $beasiswaLainnyaModel = new DynamicModel([
            'jns_beasiswa'
        ]);

        if ($model->load(Yii::$app->request->post())) {
            $model->file_beasiswa = " ";
            $id_beasmaha = $model->id_beasmaha;
            $file = UploadedFile::getInstance($model, 'file_beasiswa');
            if ($file != null) {
                $fileName = $file->baseName . '.' . $file->getExtension();
                $file->saveAs(Yii::getAlias('@backend') . '/web/Beasiswa Mahasiswa/' . $fileName);
                $model->file_beasiswa = $fileName;   
            }
            $model->save(false);
            $jns_beasiswaFromBeasiswaLainnyaModel = Yii::$app->request->post()['DynamicModel']['jns_beasiswa'];
            if ($model->jns_beasiswa == "Lainnya") {
                $model->jns_beasiswa = $jns_beasiswaFromBeasiswaLainnyaModel;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_beasmaha]);
        }

        return $this->render('create', [
            'model' => $model,
            'beasiswaLainnyaModel' => $beasiswaLainnyaModel,
        ]);
    }

    public function actionDownload($id) 
    { 
        $download = BeasiswaMahasiswa::findOne($id); 
        $path=Yii::getAlias('@backend') . '/web/Beasiswa Mahasiswa/' . $download->file_beasiswa;
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }

    public function actionExcel()
    {
        $searchModel = new BeasiswaMahasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('excel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing BeasiswaMahasiswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_beasmaha]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BeasiswaMahasiswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BeasiswaMahasiswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BeasiswaMahasiswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BeasiswaMahasiswa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionViewFile($fileName){
        $file = Yii::getAlias('@backend' . '/web/Beasiswa Mahasiswa/' . $fileName);
        return Yii::$app->response->sendFile($file, NULL, ['inline' => TRUE]);
    }
}
