<?php
namespace backend\controllers;

use Yii;
use backend\models\LpjKegiatan;
use backend\models\LpjKegiatanSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\Notifikasi;

/**
 * LpjKegiatanController implements the CRUD actions for LpjKegiatan model.
 */
class LpjKegiatanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return [
        'access' => [
          'class' => AccessControl::className(),
          'rules'=> [
            [
              'actions' => ['login', 'error'],
              'allow' => true,
            ],
            [
              'actions' => ['logout', 'index'],
              'allow' => true,
              'roles' => ['@'],
            ],
            [
              'allow' => true,
              'roles' => ['@'],
            ],
          ],
        ],
        'verbs' => [
          'class' => VerbFilter::className(),
          'actions' => [
            'logout' => ['post'],
          ],
        ],
      ];
    }

    public function actions(){
      return [
        'error' => [
          'class' => 'yii\web\ErrorAction',
        ],
      ];
    }

    /**
     * Lists all LpjKegiatan models.
     * @return mixed
     */
    public function actionIndex()
    {
      $searchModel = new LpjKegiatanSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      $dataProvider->pagination = ['pageSize' => 5];
      return $this->render('index', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
      ]);
    }


    public function actionApprove($id)
    {
      $model = LpjKegiatan::find()->where(['id_LpjKegiatan' => $id])->one();
      $notif = new Notifikasi();
      $notif->id_mhs = $model->id_mhs;
      $notif->desc = "LPJ Kegiatan Diterima";
      $model->status_request_id=1;
      $searchModel = new LpjKegiatanSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      if($model->save() && $notif->save(false)){

        return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
        ]);
        
      }
    }

       public function actionPdf($id) {
    $model = LpjKegiatan::findOne($id);
        
    

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/LPJ Kegiatan/';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

        
}


    public function actionReject($id)
    {
      $model = LpjKegiatan::find()->where(['id_LpjKegiatan' => $id])->one();
      $notif = new Notifikasi();
      $notif->id_mhs = $model->id_mhs;
      $notif->desc = "LPJ Kegiatan Ditolak";
      $model->status_request_id=2;
      $searchModel = new LpjKegiatanSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      if($model->save()){
    //disini buat savenya
       if ($notif->save(false)) {
        return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
        ]);
      }
      
    }
  }

     //Action Download
  public function actionDownload($id) 
  { 
    $download = LpjKegiatan::findOne($id); 
    $path=Yii::getAlias('@frontend') . '/web/LPJ Kegiatan/' . $download->file_lpj;
    if (file_exists($path)) {
      return Yii::$app->response->sendFile($path);
    }
  }

  public function actionPdf($id) {
    $model = LpjKegiatan::findOne($id);

    // This will need to be the path relative to the root of your app.
    $filePath = '/web/LPJ Kegiatan /';
    // Might need to change '@app' for another alias
    $completePath = Yii::getAlias('@frontend'.$filePath.'/'.$model->file_lpj);

    
    return Yii::$app->response->sendFile($completePath, $filePath, ['inline'=>true]);

  }

    /**
     * Deletes an existing LpjKompetisi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->findModel($id)->delete();

      return $this->redirect(['index']);
    }



    /**
     * Displays a single LpjKegiatan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      return $this->render('view', [
        'model' => $this->findModel($id),
      ]);
    }

    /**
     * Creates a new LpjKegiatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $model=new LpjKegiatan;
      if(isset($_POST['NamaModel']))
      {
        $model->attributes=$_POST['LpjKegiatan'];
        $model->status_request_id=0;
        if($model->save())
          $this->redirect(array('view','id'=>$model->id));
      }
      $this->render('create',array(
        'model'=>$model,
      ));
    }



    /**
     * Updates an existing LpjKegiatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdates($id)
    {
      $model=$this->loadModel($id);
      $model->status=1;
      if($model->save())
        $this->redirect(array('view','id'=>$model->id));

      $this->render('update',array(
        'model'=>$model,
      ));
    }
    protected function findModel($id)
    {
      if (($model = LpjKegiatan::findOne($id)) !== null) {
        return $model;
      } else {
        throw new NotFoundHttpException('The requested page does not exist.');
      }
    }
 /**
     * Login action.
     *
     * @return string
     */
 public function actionLogin()
 {
  if (!Yii::$app->user->isGuest) {
    return $this->goHome();
  }

  $model = new LoginForm();
  if ($model->load(Yii::$app->request->post()) && $model->login()) {
    return $this->goBack();
  } else {
    $model->password = '';

    return $this->render('login', [
      'model' => $model,
    ]);
  }
}


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
      Yii::$app->user->logout();

      return $this->goHome();
    }
  }

