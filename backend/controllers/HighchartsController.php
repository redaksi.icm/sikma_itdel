<?php

namespace backend\controllers;

use Yii;
use backend\models\Highcharts;
use backend\models\HighchartsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HighchartsController implements the CRUD actions for Highcharts model.
 */
class HighchartsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Highcharts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = Yii::$app->db->createCommand('select 
            jenis_prestasi,
            count(jenis_prestasi) as jml
            from prestasi 
            group by jenis_prestasi')->queryAll();
        return $this->render('index', [
            'dgrafik' => $data
        ]);
    }
    
    /**
     * Creates a new Highcharts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    /**
     * Finds the Highcharts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Highcharts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Highcharts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
