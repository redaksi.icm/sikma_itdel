<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "beasiswa".
 *
 * @property int $id_beasiswa
 * @property int $id_mhs
 * @property string $nama_mhs
 * @property string $alamat
 * @property string $nim_mhs
 * @property string $jlh_bersaudara
 * @property string $anak_ke
 * @property string $tanggungan
 * @property string $pekerjaan_ayah
 * @property string $pekerjaan_ibu
 * @property string $penghasilan_ayah
 * @property string $penghasilan_ibu
 * @property string $status_request_id
 */
class Beasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'nama_mhs', 'alamat', 'nim_mhs', 'jlh_bersaudara', 'anak_ke', 'tanggungan', 'pekerjaan_ayah', 'pekerjaan_ibu', 'penghasilan_ayah', 'penghasilan_ibu', 'status_request_id'], 'required'],
            [['id_mhs'], 'integer'],
            [['nama_mhs', 'alamat', 'nim_mhs', 'jlh_bersaudara', 'anak_ke', 'tanggungan', 'pekerjaan_ayah', 'pekerjaan_ibu', 'penghasilan_ayah', 'penghasilan_ibu', 'status_request_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_beasiswa' => 'Id Beasiswa',
            'id_mhs' => 'Id Mhs',
            'nama_mhs' => 'Nama Lengkap',
            'nim_mhs' => 'NIM',
                'alamat' => 'Alamat',
            'jlh_bersaudara' => 'Jumlah Bersaudara',
            'anak_ke' => 'Anak Ke',
            'tanggungan' => 'Tanggungan',
            'pekerjaan_ayah' => 'Pekerjaan Ayah',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'penghasilan_ayah' => 'Penghasilan Ayah',
            'penghasilan_ibu' => 'Penghasilan Ibu',
        ];
    }
      public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
}

