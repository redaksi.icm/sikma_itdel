<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "beasiswa_company".
 *
 * @property int $id_company
 * @property string $name_company
 * @property string $jenis_beasiswa
 * @property string $tujuan
 */
class BeasiswaCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beasiswa_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_company', 'jenis_beasiswa', 'tujuan'], 'required'],
            [['name_company', 'tujuan'], 'string', 'max' => 256],
            [['jenis_beasiswa'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_company' => 'Id Company',
            'name_company' => 'Name Company',
            'jenis_beasiswa' => 'Jenis Beasiswa',
            'tujuan' => 'Tujuan',
        ];
    }
}
