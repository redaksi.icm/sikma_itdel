<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LpjKompetisi;

/**
 * LpjKompetisiSearch represents the model behind the search form of `app\models\LpjKompetisi`.
 */
class LpjKompetisiSearch extends LpjKompetisi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_LpjKompetisi', 'id_mhs', 'status_request_id'], 'integer'],
            [['jenis_kompetisi', 'file_lpj'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LpjKompetisi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_LpjKompetisi' => $this->id_LpjKompetisi,
            'id_mhs' => $this->id_mhs,
            'status_request_id' => $this->status_request_id,
        ]);

        $query->andFilterWhere(['like', 'jenis_kompetisi', $this->jenis_kompetisi])
        ->andFilterWhere(['like', 'file_lpj', $this->file_lpj]);

        return $dataProvider;
    }
}
