<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "beasiswa_mahasiswa".
 *
 * @property int $id_beasmaha
 * @property int $id_beasiswa
 * @property int $id_mhs
 * @property string $jenis_beasiswa
 * @aproaperty strinag $atanggal_awal
 * @property string $tnggl_akhir
 *
 * @propertay Beasiswa $beaasiswa
 * @property Mahasiswa $mhs
 */
class BeasiswaMahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beasiswa_mahasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'jns_beasiswa','informasi_beasiswa', 'tnggl_awal', 'tnggl_akhir'], 'required'],
            [['file_beasiswa'], 'string', 'max' => 100],
           
            [['tanggl_awal', 'tanggl_akhir'], 'safe'],
            [['jns_beasiswa'], 'string', 'max' => 256],
            
        ];
    }

    /**
    a * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_beasmaha' => 'Id Beasmaha',
            'id_beasiswa' => 'Id Beasiswa',
            'id_mhs' => 'Id Mhs',
            'jns_beasiswa' => 'Jenis Beasiswa',
            'informasi_beasiswa' => 'Informasi Beasiswa',
             'tnggl_awal' => 'Tanggal Awal ',
            'tnggl_akahir' => 'Tanggl Akhir',
            'file_beasiswa' => 'File Beasiswa',
        ];
    }

    /**
     * Gets query for [[Beasiswa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBeasiswa()
    {
        return $this->hasOne(Beasiswa::className(), ['id_beasiswa' => 'id_beasiswa']);
    }

    /**
     * Gets query for [[Mhs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMhs()
    {
        return $this->hasOne(Mahasiswa::className(), ['id_mhs' => 'id_mhs']);
    }
    
}
