<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mhs_resign".
 *
 * @property int $id_resign
 * @property int $id_kmhs
 * @property string $nama_mhs
 * @property string $nim
 * @property string $prodi
 * @property string $alasan
 */
class MhsResign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mhs_resign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_mhs', 'nim', 'prodi', 'alasan'], 'required'],
            [['id'], 'integer'],
            [['nama_mhs', 'nim', 'prodi'], 'string', 'max' => 255],
            [['alasan'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_resign' => 'Id Resign',
            'nama_mhs' => 'Nama Mahasiswa',
            'nim' => 'NIM',
            'prodi' => 'Program Studi',
            'alasan' => 'Keterangan Resign',
        ];
    }
}
