<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lpj_kegiatan".
 *
 * @property int $id_LpjKegiatan
 * @property int $id_mhs
 * @property string $jenis_kegiatan
 * @property string $file_lpj
 * @property int $status_request_id
 */
class LpjKegiatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lpj_kegiatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'jenis_kegiatan','file_lpj', 'status_request_id'], 'required'],
            [['id_mhs', 'status_request_id'], 'integer'],
            [['jenis_kegiatan'], 'string', 'max' => 50],
            [['file_lpj'], 'file', 'skipOnEmpty' => TRUE,'extensions'=>'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_LpjKegiatan' => 'Id Lpj Kegiatan',
            'id_mhs' => 'Id Mhs',
            'jenis_kegiatan' => 'Jenis Kegiatan',
            'file_lpj' => 'LPJ Kegiatan',
        ];
    }

       public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_mhs' => 'id_mhs']);
    }
}
