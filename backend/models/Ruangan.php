<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ruangan".
 *
 * @property int $id_ruangan
 * @property int $id_mhs
 * @property string $jns_ruangan
 * @property string $tujuan
 * @property string $waktu_peminjaman
 * @property string $waktu_akhir_peminjaman
 * @property int $status_request_id
 *
 * @property StatusRequest $statusRequest
 */
class Ruangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'status_request_id'], 'integer'],
            [['jns_ruangan', 'tujuan', 'waktu_peminjaman', 'waktu_akhir_peminjaman', 'status_request_id'], 'required'],
            [['waktu_peminjaman', 'waktu_akhir_peminjaman'], 'safe'],
            [['jns_ruangan', 'tujuan'], 'string', 'max' => 100],
            [['status_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusRequest::className(), 'targetAttribute' => ['status_request_id' => 'status_request_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ruangan' => 'Id Ruangan',
            'id_mhs' => 'Id Mhs',
            'jns_ruangan' => 'Ruangan',
            'tujuan' => 'Tujuan',
            'waktu_peminjaman' => 'Waktu Peminjaman',
            'waktu_akhir_peminjaman' => 'Waktu Akhir Peminjaman',
            'status_request_id' => 'Status Request ID',
        ];
    }

    /**
     * Gets query for [[StatusRequest]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_mhs' => 'id_mhs']);
    }
}
