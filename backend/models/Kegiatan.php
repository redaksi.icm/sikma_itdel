<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kegiatan".
 *
 * @property int $id_kegiatan
 * @property int $id_mhs
 * @property string $jenis_kegiatan
 * @property string $upload_proposal

 *
 * @property Ruangan[] $ruangans
 */
class Kegiatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kegiatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'jenis_kegiatan', 'upload_proposal'], 'required'],
            [['id_mhs'], 'integer'],
            [['jenis_kegiatan'], 'string', 'max' => 255],
            [['upload_proposal'],'file','extensions' => 'pdf'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kegiatan' => 'Id Kegiatan',
            'id_mhs' => 'Id Mhs',
            'jenis_kegiatan' => 'Jenis Kegiatan',
            'upload_proposal' => 'Upload Proposal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuangans()
    {
        return $this->hasMany(Ruangan::className(), ['id_kegiatan' => 'id_kegiatan']);
    }
    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_mhs' => 'id_mhs']);
    }
}

