<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BeasiswaMahasiswa;

/**
 * BeasiswaMahasiswaSearch represents the model behind the search form of `backend\models\BeasiswaMahasiswa`.
 */
class BeasiswaMahasiswaSearch extends BeasiswaMahasiswa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_beasmaha'], 'integer'],
            [['jns_beasiswa','informasi_beasiswa','tnggl_awal', 'tnggl_akhir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BeasiswaMahasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_beasmaha' => $this->id_beasmaha,
            
            'tnggl_awal' => $this->tnggl_awal,
            'tnggl_akhir' => $this->tnggl_akhir,
        ]);

        $query->andFilterWhere(['like', 'jns_beasiswa', $this->jns_beasiswa]);

        return $dataProvider;
    }
}
