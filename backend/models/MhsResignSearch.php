<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MhsResign;

/**
 * MhsResignSearch represents the model behind the search form of `app\models\MhsResign`.
 */
class MhsResignSearch extends MhsResign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_resign', 'id'], 'integer'],
            [['nama_mhs', 'nim', 'prodi', 'alasan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchExcel($params)
    {
        $query = MhsResign::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_resign' => $this->id_resign,
        ]);

        $query->andFilterWhere(['like', 'nama_mhs', $this->nama_mhs])
            ->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'alasan', $this->alasan]);

        return $dataProvider;
    }


    public function search($params)
    {
        $query = MhsResign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_resign' => $this->id_resign,
        ]);

        $query->andFilterWhere(['like', 'nama_mhs', $this->nama_mhs])
            ->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'alasan', $this->alasan]);

        return $dataProvider;
    }
}
