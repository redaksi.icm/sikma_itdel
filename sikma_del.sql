-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Jul 2020 pada 11.07
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikma_del`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa`
--

CREATE TABLE `beasiswa` (
  `id_beasiswa` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `nama_mhs` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `nim_mhs` varchar(50) NOT NULL,
  `jlh_bersaudara` int(11) NOT NULL,
  `anak_ke` int(11) NOT NULL,
  `tanggungan` int(11) NOT NULL,
  `pekerjaan_ayah` varchar(50) NOT NULL,
  `pekerjaan_ibu` varchar(50) NOT NULL,
  `penghasilan_ayah` int(11) NOT NULL,
  `penghasilan_ibu` int(11) NOT NULL,
  `status_request_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `beasiswa`
--

INSERT INTO `beasiswa` (`id_beasiswa`, `id_mhs`, `nama_mhs`, `alamat`, `nim_mhs`, `jlh_bersaudara`, `anak_ke`, `tanggungan`, `pekerjaan_ayah`, `pekerjaan_ibu`, `penghasilan_ayah`, `penghasilan_ibu`, `status_request_id`, `deleted`, `deleted_at`, `deleted_by`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 0, 'Nopri Yendry S', 'Silaen', '11317011', 4, 3, 2, 'Polisi', 'Guru', 20000000, 6000000, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 0, 'Kevin', 'Tarutung', '11317002', 2, 1, 2, 'Guru', 'Guru', 3000000, 3000000, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 0, 'Layla', 'Sigumpar', '11317061', 5, 3, 3, 'PNS', 'PNS', 3500000, 2000000, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 0, 'Amsal', 'Tarutung', '11317022', 1, 1, 1, 'PNS', 'PNS', 3000000, 3000000, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 0, 'Sweta', 'Tarutung', '11317008', 5, 3, 3, '+', 'Guru', 0, 3000000, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa_company`
--

CREATE TABLE `beasiswa_company` (
  `id_company` int(11) NOT NULL,
  `name_company` varchar(256) NOT NULL,
  `jenis_beasiswa` varchar(255) NOT NULL,
  `tujuan` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `beasiswa_company`
--

INSERT INTO `beasiswa_company` (`id_company`, `name_company`, `jenis_beasiswa`, `tujuan`) VALUES
(1, 'Tanoto Foundation', 'Tanoto Foundation', 'Tanoto Foundation'),
(2, 'Ikatan Alumni', 'Ikatan Alumni', 'Ikatan Alumni'),
(3, 'Djarum Super', 'Beasiswa Djarum', 'Beasiswa Djarum'),
(4, 'Lainnya', 'Lainnya', 'Lainnya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa_mahasiswa`
--

CREATE TABLE `beasiswa_mahasiswa` (
  `id_beasmaha` int(11) NOT NULL,
  `jns_beasiswa` varchar(256) NOT NULL,
  `informasi_beasiswa` varchar(500) NOT NULL,
  `file_beasiswa` varchar(100) NOT NULL,
  `tnggl_awal` date NOT NULL,
  `tnggl_akhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `beasiswa_mahasiswa`
--

INSERT INTO `beasiswa_mahasiswa` (`id_beasmaha`, `jns_beasiswa`, `informasi_beasiswa`, `file_beasiswa`, `tnggl_awal`, `tnggl_akhir`) VALUES
(2, 'Ikatan Alumni', 'Telah dibuka bagi mahasiswa Angkatan 2018 beasiswa Ikatan Alumni. Jika berminat silahkan melengkapi data diri dengan mengisi nya melalui sistem kemahasiswaan IT Del.\r\nTerimakasih. \r\nSalam', '', '2020-04-07', '2020-04-10'),
(3, 'Beasiswa Djarum', 'Dear Mahasiswa,\r\nTelah dibuka pendaftaran beasiswa dari Beasiswa Djarum.\r\nBagi yang ingin mendaftarkan diri, silahkan mendaftar dengan mengisi data diei melalui Sistem IInformasi Kemahasiswaan.\r\nTerimakasih.', '', '2020-04-18', '2020-04-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kegiatan` varchar(255) NOT NULL,
  `upload_proposal` varchar(255) NOT NULL,
  `status_request_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `id_mhs`, `jenis_kegiatan`, `upload_proposal`, `status_request_id`) VALUES
(1, 1, 'Del Art Days', 'Proposal_Kegiatan DAD1.pdf', 1),
(2, 1, 'Del  Bersatus', 'Sertifikat_UIUX2.pdf', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kemahasiswaan`
--

CREATE TABLE `kemahasiswaan` (
  `id_kmhs` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama_kmhs` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kompetisi`
--

CREATE TABLE `kompetisi` (
  `id_kompetisi` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kompetisi` varchar(50) NOT NULL,
  `upload_proposal` varchar(50) NOT NULL,
  `status_request_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kompetisi`
--

INSERT INTO `kompetisi` (`id_kompetisi`, `id_mhs`, `jenis_kompetisi`, `upload_proposal`, `status_request_id`) VALUES
(1, 1, 'Computational Thinking IT', 'LPJ_Agricode Programming Contest1.pdf', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpj_kegiatan`
--

CREATE TABLE `lpj_kegiatan` (
  `id_LpjKegiatan` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kegiatan` varchar(50) NOT NULL,
  `file_lpj` varchar(500) NOT NULL,
  `status_request_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpj_kompetisi`
--

CREATE TABLE `lpj_kompetisi` (
  `id_LpjKompetisi` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_kompetisi` varchar(50) NOT NULL,
  `file_lpj` varchar(500) NOT NULL,
  `status_request_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lpj_kompetisi`
--

INSERT INTO `lpj_kompetisi` (`id_LpjKompetisi`, `id_mhs`, `jenis_kompetisi`, `file_lpj`, `status_request_id`) VALUES
(1, 1, 'Programming Goleng', 'LPJ_VokalSolo.pdf', 2),
(2, 1, 'Del Tonight Show', 'LPJ_TonightShow.pdf', 1),
(3, 1, 'Doa Bersama', 'LPJ_VokalSolo.pdf', 2),
(4, 1, 'Coding #StayatHome', 'LPJ_#DirumahAja.pdf', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `nama_mhs` varchar(50) NOT NULL,
  `nim_mhs` varchar(50) NOT NULL,
  `prodi` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mhs_resign`
--

CREATE TABLE `mhs_resign` (
  `id_resign` int(11) NOT NULL,
  `id_kmhs` int(11) NOT NULL,
  `nama_mhs` varchar(255) NOT NULL,
  `nim` varchar(255) NOT NULL,
  `prodi` varchar(255) NOT NULL,
  `alasan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mhs_resign`
--

INSERT INTO `mhs_resign` (`id_resign`, `id_kmhs`, `nama_mhs`, `nim`, `prodi`, `alasan`) VALUES
(2, 0, 'Sweta Hutauruk', '11317008', 'D3 Teknologi Informasi', 'Tidak Masuk Kelas Berturut');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1580888610),
('m130524_201442_init', 1580888615),
('m190124_110200_add_verification_token_column_to_user_table', 1580888616);

-- --------------------------------------------------------

--
-- Struktur dari tabel `new_user`
--

CREATE TABLE `new_user` (
  `id_mhs` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `authKey` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `new_user`
--

INSERT INTO `new_user` (`id_mhs`, `firstname`, `lastname`, `username`, `password`, `authKey`) VALUES
(1, 'jola', 'sinambela', 'jola', 'jola123', '12345a'),
(2, 'jhosua', 'sinambela', 'jhosua', 'jhosua656', '12345ww'),
(5, 'user', 'user', 'user', 'user123', 'user12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi`
--

CREATE TABLE `notifikasi` (
  `notif_id` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `notifikasi`
--

INSERT INTO `notifikasi` (`notif_id`, `id_mhs`, `desc`, `status`) VALUES
(1, 1, 'LPJ Kompetisi Diterima', 0),
(2, 1, 'Request Prestasi Anda Diterima', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `jenis_prestasi` varchar(255) NOT NULL,
  `tgl_awal_kegiatan` date NOT NULL,
  `tgl_akhir_kegiatan` date NOT NULL,
  `upload_file` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `status_request_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `program_studi`
--

CREATE TABLE `program_studi` (
  `prodi_id` int(2) NOT NULL,
  `program_studi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `program_studi`
--

INSERT INTO `program_studi` (`prodi_id`, `program_studi`) VALUES
(1, 'D3 Teknologi Informasi'),
(2, 'D3 Teknik Komputer'),
(3, 'D4 Teknologi Rekayasa Perangkat Lunak'),
(4, 'S1 Sistem Informasi'),
(5, 'S1 Teknik Bioproses'),
(6, 'S1 Teknik Informatika'),
(7, 'S1 Manajemen Rekayasa'),
(8, 'S1 Teknik Elektro');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`role_id`, `nama`) VALUES
(1, 'admin'),
(2, 'users');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangan`
--

CREATE TABLE `ruangan` (
  `id_ruangan` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `jns_ruangan` varchar(100) NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `waktu_peminjaman` datetime NOT NULL,
  `waktu_akhir_peminjaman` datetime NOT NULL,
  `status_request_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_request`
--

CREATE TABLE `status_request` (
  `status_request_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status_request`
--

INSERT INTO `status_request` (`status_request_id`, `name`) VALUES
(0, 'Menunggu'),
(1, 'Disetujui'),
(2, 'Ditolak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'jhosua', 'eq1TdscAp-q4TZUMjK3PxFy9-tssifUH', '$2y$13$xxaPz.xsI1yAZ4Txd4/sC.qwoL5s7C4f9P2LBxsYyF59laXKjRUkq', NULL, 'jhosuagaruda389@gmail.com', 9, 1580888730, 1580888730, 'RNHoMirnEWilLmqNtcTwONGhfScwrfPL_1580888730'),
(2, 'jhosuasinambela', 'PhWOP__1PJ6MGWekoLLhjI1vhxn7CbOb', '$2y$13$qlhyCi/1e.wJ2Hw2dNgeZ.l1NIhMALe7sRXOjlQiDiGuR7aYGdocS', NULL, 'januarsimson@gmail.com', 10, 1580889151, 1580889151, NULL),
(3, 'nopri', '4mjkqnQnxMvRLl4agqjNoYeB7S0Imu_X', '$2y$13$VAGTsrVDACzUR.8lW/IOR.tN0N2YKTgVvQXJgesr1KqICgbkCiGOO', NULL, 'nopri@gmail.com', 10, 1584073830, 1584073830, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `beasiswa`
--
ALTER TABLE `beasiswa`
  ADD PRIMARY KEY (`id_beasiswa`);

--
-- Indeks untuk tabel `beasiswa_mahasiswa`
--
ALTER TABLE `beasiswa_mahasiswa`
  ADD PRIMARY KEY (`id_beasmaha`);

--
-- Indeks untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indeks untuk tabel `kemahasiswaan`
--
ALTER TABLE `kemahasiswaan`
  ADD PRIMARY KEY (`id_kmhs`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `kompetisi`
--
ALTER TABLE `kompetisi`
  ADD PRIMARY KEY (`id_kompetisi`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indeks untuk tabel `lpj_kegiatan`
--
ALTER TABLE `lpj_kegiatan`
  ADD PRIMARY KEY (`id_LpjKegiatan`);

--
-- Indeks untuk tabel `lpj_kompetisi`
--
ALTER TABLE `lpj_kompetisi`
  ADD PRIMARY KEY (`id_LpjKompetisi`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `mhs_resign`
--
ALTER TABLE `mhs_resign`
  ADD PRIMARY KEY (`id_resign`),
  ADD KEY `id_kmhs` (`id_kmhs`);

--
-- Indeks untuk tabel `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indeks untuk tabel `new_user`
--
ALTER TABLE `new_user`
  ADD PRIMARY KEY (`id_mhs`);

--
-- Indeks untuk tabel `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indeks untuk tabel `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`),
  ADD KEY `id_mhs` (`id_mhs`);

--
-- Indeks untuk tabel `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`prodi_id`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indeks untuk tabel `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`id_ruangan`),
  ADD KEY `id_kegiatan` (`id_kegiatan`);

--
-- Indeks untuk tabel `status_request`
--
ALTER TABLE `status_request`
  ADD PRIMARY KEY (`status_request_id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `beasiswa`
--
ALTER TABLE `beasiswa`
  MODIFY `id_beasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `beasiswa_mahasiswa`
--
ALTER TABLE `beasiswa_mahasiswa`
  MODIFY `id_beasmaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kemahasiswaan`
--
ALTER TABLE `kemahasiswaan`
  MODIFY `id_kmhs` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kompetisi`
--
ALTER TABLE `kompetisi`
  MODIFY `id_kompetisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `lpj_kegiatan`
--
ALTER TABLE `lpj_kegiatan`
  MODIFY `id_LpjKegiatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `lpj_kompetisi`
--
ALTER TABLE `lpj_kompetisi`
  MODIFY `id_LpjKompetisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mhs` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mhs_resign`
--
ALTER TABLE `mhs_resign`
  MODIFY `id_resign` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `new_user`
--
ALTER TABLE `new_user`
  MODIFY `id_mhs` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id_prestasi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `program_studi`
--
ALTER TABLE `program_studi`
  MODIFY `prodi_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `id_ruangan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `status_request`
--
ALTER TABLE `status_request`
  MODIFY `status_request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
