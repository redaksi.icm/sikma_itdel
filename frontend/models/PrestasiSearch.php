<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prestasi;

/**
 * PrestasiSearch represents the model behind the search form of `backend\models\Prestasi`.
 */
class PrestasiSearch extends Prestasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_prestasi', 'id_mhs'], 'integer'],
            [['jenis_prestasi',  'tgl_awal_kegiatan', 'tgl_akhir_kegiatan', 'upload_file', 'keterangan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchExcel($params)
    {
        $query = Prestasi::find()->where(['dim_id' => Yii::$app->user->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

         // grid filtering conditions
<<<<<<< frontend/models/PrestasiSearch.php
        $query->andFilterWhere([
=======
         $query->andFilterWhere([
>>>>>>> frontend/models/PrestasiSearch.php

            'tgl_awal_kegiatan' => $this->tgl_awal_kegiatan,
            'tgl_akhir_kegiatan' => $this->tgl_akhir_kegiatan,
        ]);

        $query->andFilterWhere(['like', 'jenis_prestasi', $this->jenis_prestasi])
<<<<<<< frontend/models/PrestasiSearch.php
        ->andFilterWhere(['like', 'upload_file', $this->upload_file])
        ->andFilterWhere(['like', 'keterangan', $this->keterangan]);
=======
            ->andFilterWhere(['like', 'upload_file', $this->upload_file])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);
>>>>>>> frontend/models/PrestasiSearch.php

        return $dataProvider;
    }

    public function search($params)
    {
        $query = Prestasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([

            'id_prestasi' => $this->id_prestasi,
            'id_mhs'=> $this->id_mhs,
        ]);

        $query->andFilterWhere(['like', 'jenis_prestasi', $this->jenis_prestasi])
<<<<<<< frontend/models/PrestasiSearch.php
        ->andFilterWhere(['like', 'upload_file', $this->upload_file])
        ->andFilterWhere(['like', 'keterangan', $this->keterangan]);
        
=======
            ->andFilterWhere(['like', 'upload_file', $this->upload_file])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);
            
>>>>>>> frontend/models/PrestasiSearch.php

        return $dataProvider;
    }
}