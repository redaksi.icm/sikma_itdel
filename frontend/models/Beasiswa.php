<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "beasiswa".
 *
 * @property int $id_beasiswa
 * @property int $id_mhs
 * @property string $nama_mhs
 * @property string $alamat
 * @property string $nim_mhs
 * @property int $jlh_bersaudara
 * @property int $anak_ke
 * @property int $tanggungan
 * @property string $pekerjaan_ayah
 * @property string $pekerjaan_ibu
 * @property int $penghasilan_ayah
 * @property int $penghasilan_ibu
 */
class Beasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'nama_mhs', 'alamat', 'nim_mhs', 'jlh_bersaudara', 'anak_ke', 'tanggungan', 'pekerjaan_ayah', 'pekerjaan_ibu', 'penghasilan_ayah', 'penghasilan_ibu'], 'required'],
            [['id_mhs'], 'integer'],
            [['nama_mhs', 'alamat', 'nim_mhs', 'pekerjaan_ayah', 'pekerjaan_ibu','jlh_bersaudara', 'anak_ke', 'tanggungan', 'penghasilan_ayah', 'penghasilan_ibu'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_beasiswa' => 'Id Beasiswa',
            'id_mhs' => 'Id Mhs',
            'nama_mhs' => 'Nama Lengkap',
            'alamat' => 'Alamat',
            'nim_mhs' => 'NIM',
            'jlh_bersaudara' => 'Jlh Bersaudara',
            'anak_ke' => 'Anak Ke',
            'tanggungan' => 'Tanggungan',
            'pekerjaan_ayah' => 'Pekerjaan Ayah',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'penghasilan_ayah' => 'Penghasilan Ayah',
            'penghasilan_ibu' => 'Penghasilan Ibu',
        ];
    }

    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
}

