<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lpj_kompetisi".
 *
 * @property int $id_mhs
 * @property string $jenis_kompetisi
 * @property string $file_lpj
 * @property int $id_LpjKompetisi
 
 */
class LpjKompetisi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lpj_kompetisi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_kompetisi', 'file_lpj'], 'required'],
            [['jenis_kompetisi'], 'safe'],
            [['file_lpj'], 'file', 'skipOnEmpty' => true, 'extensions'=>'pdf'],
            [['jenis_kompetisi'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_LpjKompetisi' => 'Id Lpj Kompetisi',
            'id_mhs' => 'Id Mhs',
            'jenis_kompetisi' => 'Jenis Kompetisi',
            'file_lpj' => 'File LPJ',
            'status_request_id' => 'Status',
        ];
    }
    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
}
