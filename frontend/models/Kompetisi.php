<?php

namespace app\models;

use Yii;
use app\components\NotificationBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "kompetisi".
 *
 * @property int $id_kompetisi
 * @property int $id_mhs
 * @property string $jenis_kompetisi
 * @property string $upload_proposal
 */
class Kompetisi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $file;

    public static function tableName()
    {
        return 'kompetisi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_kompetisi' , 'upload_proposal'], 'required'],
            [['file'],'file'],
            [['jenis_kompetisi', 'upload_proposal'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kompetisi' => 'Id Kompetisi',
            'id_mhs' => 'Id Mhs',
            'jenis_kompetisi' => 'Jenis Kompetisi',
            'file' => 'Upload Proposal',
        ];
    }


    

    public function getStatusRequest()
    {
        return $this->hasOne(StatusRequest::className(), ['status_request_id' => 'status_request_id']);
    }
}
