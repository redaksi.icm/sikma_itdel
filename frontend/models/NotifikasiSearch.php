<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Notifikasi;

/**
 * NotifikasiSearch represents the model behind the search form of `frontend\models\Notifikasi`.
 */
class NotifikasiSearch extends Notifikasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notif_id', 'id_mhs', 'desc', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notifikasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'notif_id' => $this->notif_id,
            'id_mhs' => $this->id_mhs,
            'desc' => $this->desc,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
