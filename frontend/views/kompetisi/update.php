<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Kompetisi */

$this->title = 'Update Data Kompetisi';
$this->params['breadcrumbs'][] = ['label' => 'Kompetisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kompetisi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
