<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Kompetisi */

$this->title = 'Data Kompetisi';
$this->params['breadcrumbs'][] = ['label' => 'LPJ Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jenis_kompetisi',
            ['attribute' => 'upload_proposal',
            'label' => 'Proposal Kompetisi',
            'format' => 'html',
            'value' => 
            function($model){
                return Html::a($model->upload_proposal, ['kompetisi/download', 'id'=> $model->id_kompetisi]);
            }
        ],
        ['label' => 'Status Request', 'value' => $model->statusRequest->name],
    ],
]) ?>

</div>
