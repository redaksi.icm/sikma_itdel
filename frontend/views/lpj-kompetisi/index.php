<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\ToolsColumn;


$this->title = 'LPJ Kompetisi';
$this->params['breadcrumbs'][] = ['label' => 'LPJ Kompetisi', 'url' => ['index']];
$uiHelper=\Yii::$app->uiHelper;
?>

<div class="lpj-kegiatan-index">

  <?= $uiHelper->renderContentHeader($this->title);?>
  <?php

  $toolbarItemMenu =  
  "<a href='".Url::to(['create'])."' class='btn btn-success'><i class='fa fa-book'></i><span class='toolbar-label'>Upload LPJ</span></a>
  "
  ;

  ?>


  <?=Yii::$app->uiHelper->renderToolbar([
    'pull-left' => true,
    'groupTemplate' => ['groupStatusExpired'],
    'groups' => [
      'groupStatusExpired' => [
        'template' => ['filterStatus'],
        'buttons' => [
          'filterStatus' => $toolbarItemMenu,
        ]
      ],
    ],
  ]) ?>


  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'jenis_kompetisi',
      ['attribute' => 'file_lpj',
      'label' => 'LPJ Kompetisi',
      'format' => 'html',
      'value' => 
      function($model){
        return Html::a($model->file_lpj, ['lpj-kompetisi/download', 'id'=> $model->id_LpjKompetisi]);
      }
    ],
    ['attribute' => 'status_request_id',
    'label' => 'Status Request',
    'value' => function($model){
      if(is_null($model->statusRequest['name'])){
        return '-';
      }else{
        return $model->statusRequest['name'];
      }
    }
  ],

  ['class' => 'common\components\ToolsColumn',
  'template' => '{view} {edit}',
  'header' => 'Aksi',
  'buttons' => [
    'view' => function ($url, $model){
      return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
    },
    'edit' => function ($url, $model){
      if ($model->status_request_id == 1 || $model->status_request_id == 2) {
        return "";
      }else{
        return ToolsColumn::renderCustomButton($url, $model, 'Edit', 'fa fa-times');
      }
    },
  ],
  'urlCreator' => function ($action, $model, $key, $index){
    if ($action === 'view') {
      return Url::toRoute(['pdf', 'id' => $key]);
    }else if ($action === 'edit') {
      return Url::toRoute(['update', 'id' => $key]);
    }

  }
],
],

]); ?>

</div>
