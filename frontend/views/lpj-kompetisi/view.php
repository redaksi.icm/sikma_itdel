<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LPJKompetisi */


$this->title = 'Data LPJ';

$this->params['breadcrumbs'][] = ['label' => 'LPJ Kompetisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lpj-kompetisi-view">
 <h1><?= Html::encode($this->title) ?></h1>


 <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'jenis_kompetisi',
        ['attribute' => 'file_lpj',
        'label' => 'LPJ Kompetisi',
        'format' => 'html',
        'value' => 
        function($model){

            return Html::a($model->file_lpj, ['lpj-kompetisi/download', 'id'=> $model->id_LpjKompetisi]);

        }
    ],
    ['label' => 'Status Request', 'value' => $model->statusRequest->name],
],
]) ?>

</div>
