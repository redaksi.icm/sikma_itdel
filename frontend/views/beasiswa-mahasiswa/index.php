<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\BeasiswaMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Beasiswa Mahasiswas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beasiswa-mahasiswa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Beasiswa Mahasiswa'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Export ke Excel', ['excel'], ['class' => 'btn btn-primary']) ?> 
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_beasmaha',
            // 'id_beasiswa',
            // 'id_mhs',
            'jns_beasiswa',
            'informasi_beasiswa',
            'tnggl_awal',
            'tnggl_akhir',
            ['attribute' => 'file_beasiswa',
            'label' => 'File Beasiswa',
            'format' => 'html',
            'value' => 
            function($model){
                return Html::a($model->file_beasiswa, ['beasiswa-mahasiswa/download', 'id'=> $model->id_beasmaha]);
            }
        ],

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
