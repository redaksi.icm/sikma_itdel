<?php
 \backend\modules\kmsx\assets\web\php\ExcelGrid::widget([ 
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
 			//'extension'=>'xlsx',
 			'filename'=>'DataPerilaku',
 			'properties' =>[
 			//'creator' =>'',
 			//'title'  => '',
 			//'subject'  => '',
 			//'category' => '',
			//'keywords'  => '',
 			//'manager'  => '',
 		],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
 			'nim',
 			'nama_mhs',
 			'pelanggaran', 
 			'tanggal',
            'sanksi',
            'keterangan',
        ],
    ]);
?>