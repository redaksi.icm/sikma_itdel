<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'SIK';
?>
    

    <div id="wrapper">
        <!-- start header -->
       
        <!-- end header -->
        <section id="featured">
            <!-- start slider -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Slider -->
                        <div id="main-slider" class="flexslider">
                            <ul class="slides">
                                <li>
                                    <img src="img/slides/Robot.jpg" alt="" />
                                    <div class="flex-caption">
                                        <h3>Kontes Robot Indonesia (KRI)</h3>
                                        <p>merupakan ajang kompetisi rancang bangun dan rekayasa dalam bidang robotika</p>
                                        <a  class="hover-wrap fancybox" data-fancybox-group="slider" title="KRI" href="img/works/11.jpg" >Read more
                                        <span class="overlay-img"></span></a>
                                        <img src="img/works/11.jpg" alt="Pada tanggal 4-6 April 2019, beberapa dari Mahasiswa/i Institut Teknologi Del mengikuti KRI Regional 1 di Universitas Teknokrat Indonesia, Bandar Lampung. Dari 6 divisi yang diperlombakan, Institut Teknologi Del mengikuti 3 divisi yaitu : 1.Kontes Robot Pemadam Api Indonesia (KRPAI); 2.Kontes Robot Seni Tari Indonesia (KRSTI); 3.Kontes Robot Sepak Bola Indonesia (KRSBI) Beroda.">

                                    </div>
                                </li>
                                <li>
                                    <img src="img/slides/unnamed.jpg" alt="" />
                                    <div class="flex-caption">
                                        <h3>Sharing Session  </h3>
                                        <p>Sharing Session Prof. Intan Ahmad, PhD (Dirjen Pembelajaran dan Kemahasiswaan KEMENRISTEK DIKTI).</p>
                                        <a  class="hover-wrap fancybox" data-fancybox-group="slider" title="SHARING SESSION" href="img/works/Ibuarlinta.jpg" >Read more
                                        <span class="overlay-img"></span></a>
                                        <img src="img/works/Ibuarlinta.jpg" alt="Selasa, 19 April 2016 Institut Teknologi Del (IT Del) menerima kunjungan Dirjen Pembelajaran dan Kemahasiswaan KEMENRISTEK DIKTI Prof. Intan Ahmad, PhD. Kunjungan Prof. Intan disambut langsung oleh Rektor IT Del Prof. Roberd Saragih beserta jajarannya.Dalam kunjungannya ke IT Del, Prof. Intan berbagi pengalaman dan ilmu terkait pengelolaan perguruan tinggi kepada para dosen IT Del. Bertempat di gedung 512 kampus IT Del, Prof. Intan Ahmad membawakan materi terkait Program Pembelajaran dan Kemahasiswaan. ">

                                    </div>
                                </li>
                                <li>
                                    <img src="img/slides/Struktur-inti.jpg" alt="" />
                                    <div class="flex-caption">
                                        <h3>Struktur Wakil Rektor Bidang Akademik dan Kemahasiswaan (WR AM)</h3>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- end slider -->
                    </div>
                </div>
            </div>



        </section>
      <section class="callaction">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="big-cta">
                    <div class="cta-text">
                        <h2 style="align: left"><span>Pengumuman</span> Kemahasiswaan</h2>
                        <div class="row" style="color:black;">
                            <?php
                                                       foreach ($beasiswa as $key => $value) {

                            if(strtotime($value->tnggl_akhir)>=strtotime(date('Y-m-d'))){
                            echo "
                            <div class='col-lg-5'>";
                                echo "
                                <ul id='list-pengumuman' class='event-list' style='list-style-type:none'>";

                                    echo "<p><b><li style='display: list-item;opacity: 1' ;><span class='fa fa-bullhorn'>(".$value->tnggl_akhir.")".$value->jns_beasiswa."</span></li></b></p>";

                                    echo "<li>".$value->informasi_beasiswa."</li>";

                                    echo "<li>".nl2br(Html::a($value->file_beasiswa, ['beasiswa-mahasiswa/download', 'id'=>
                                        $value->id_beasmaha]))."<li>";
                                        echo "</li>
                                </ul>
                            </div>
                            " ;
                            }else{

                            }

                            }

                            ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="box">
                                    <div class="box-gray aligncenter">
                                        <h4>MARTUHAN</h4>
                                        <div class="icon">
                                            <i class="fa fa-user fa-3x"></i>
                                        </div>
                                        <p>
                                            Percaya kepada kepada Tuhan Yang Maha Esa dan selalu mengandalkan Tuhan
                                        </p>

                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="box">
                                    <div class="box-gray aligncenter">
                                        <h4>MARROHA</h4>
                                        <div class="icon">
                                            <i class="fa fa-group fa-3x"></i>
                                        </div>
                                        <p>
                                            Keseluruhan cara seseorang bereaksi dan berinteraksi dengan orang lain
                                        </p>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="box">
                                    <div class="box-gray aligncenter">
                                        <h4>MARBISUK</h4>
                                        <div class="icon">
                                            <i class="fa fa-heartbeat fa-3x"></i>
                                        </div>
                                        <p>
                                            Selalu menggunakan akal budinya atas segala yang akan di kerjakan
                                        </p>

                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- divider -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="solidline">
                        </div>
                    </div>
                </div>
                <!-- end divider -->
                <!-- Portfolio Projects -->
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="heading">Recent Works</h4>
                        <div class="row">
                            <section id="projects">
                                <ul id="thumbs" class="portfolio">
                                    <!-- Item Project and Filter Name -->
                                    <li class="col-lg-3 design" data-id="id-0" data-type="web">
                                        <div class="item-thumbs">
                                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Shope League" href="img/works/FB.jpg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                                            <!-- Thumb Image and Description -->
                                            <img src="img/works/FB.jpg" alt="Shopee Code League is happening from 8 June to 1 August. We are calling out to all undergraduate students and professionals across the region to join us in a series of coding challenges and put your coding skills to the test.">
                                        </div>
                                    </li>
                                    <!-- End Item Project -->
                                    <!-- Item Project and Filter Name -->
                                    <li class="item-thumbs col-lg-3 design" data-id="id-1" data-type="icon">
                                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Startupfest" href="img/works/10.jpeg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                                        <!-- Thumb Image and Description -->
                                        <img src="img/works/10.jpeg" alt="Pagelaran tersebut  menghadirkan program-program seperti keynote dan panel session dengan pembicara seperti Front-End Engineer Canva Steven Sinatra, Managing Partner Kejora Ventures Andy Zain.">
                                    </li>
                                    <!-- End Item Project -->
                                    <!-- Item Project and Filter Name -->
                                    <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="web">
                                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Kerjasama Del" href="img/works/16.jpg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                                        <!-- Thumb Image and Description -->
                                        <img src="img/works/16.jpg" alt="Sitoluama, 11 Juli 2019, telah dilakukan penandatangan Nota Kesepahaman (MoU) antara IT Del dengan PT Solusi Transportasi Indonesia atau yang lebih dikenal dengen sebutan Grab. MoU ini berisikan kerja sama di bidang pengembangan talenta mahasiswa, yang mana Grab akan menyediakan program khusus untuk pengembangan talenta dan perekrutan mahasiswa IT Del. .">
                                    </li>
                                    <!-- End Item Project -->
                                    <!-- Item Project and Filter Name -->
                                    <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="web">
                                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Menko Bidang Kemaritiman dan Investasi dan Menteri Pariwisata dan Ekonomi Kreatif" href="img/works/web.jpg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                                        <!-- Thumb Image and Description -->
                                        <img src="img/works/web.jpg" alt="Dalam Rakor tersebut membahas tentang aktifitas dan persiapan yang telah dilakukan oleh BODT, pembehanan fisik pada dermaga Balige, pengecekkan dermaga dan kapal yang akan digunakan oleh Raja dan Ratu Belanda, Pelaporan perkembangan terbaru yang telah dilakukan oleh Deputi Bidang Koordinasi Parawisata dan Ekonomi Kreatif..">
                                    </li>
                                    <!-- End Item Project -->
                                </ul>
                            </section>
                        </div>
                    </div>
                </div>

            </div>
        </section>