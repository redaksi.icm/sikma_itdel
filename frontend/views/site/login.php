<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<style>
 .login-background{
 font-family: arial;
 
 padding: 10px;
 box-shadow: 100px 100px 100px #FFFFFF;
 border-radius: 6spx;
 width: 300px;
 


 height: auto;
 margin: 150px auto;
 text-align: center;

 
 }
 .limiter {
  width: 100%;
  margin: 0 auto;
}

.container-login100 {
  width: 100% ;
  height: 100%;
    

  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  
  
  justify-content: center;
  
  padding: 150px auto;

  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;
  
    
}
.wrap-login100 {
  width: 390px;
  border-radius: 10px;
  overflow: hidden;

  background: transparent;
}


/*------------------------------------------------------------------
[ Form ]*/

.login100-form {
  width: 100%;
  border-radius: 10px;
  background-color: #fff;
}
 
   


</style>

<div class="limiter">
<div class="container-login100" style="background-image: url('../image/del2.jpg')">

            <div class="wrap-login100 p-t-30 p-b-50">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
        
        <form>
            <div class="login-background">
        <div class="col-md-20 box box-radius" >  
                <h4><b>Sistem Informasi Kemahasiswaan<b></h4>
        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Sign in', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>
    </div>
        </form>
    
    
    
        
            
                

           


        
        

            



        <?php ActiveForm::end(); ?>

        
</div>

        </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->