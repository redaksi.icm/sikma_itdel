<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LPJKegiatan */

$this->title = 'Upload LPJ Kegiatan';
$this->params['breadcrumbs'][] = ['label' => 'Lpj Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpjkegiatan-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
