<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LPJKegiatan */

// $this->title = $model->jenis_kegiatan;
$this->title = 'Data LPJ';
$this->params['breadcrumbs'][] = ['label' => 'LPJ Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lpj-kegiatan-view">

	<h1><?= Html::encode($this->title) ?></h1>


	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
            // 'id_LpjKegiatan',
			'jenis_kegiatan',
			['attribute' => 'file_lpj',
			'label' => 'LPJ Kegiatan',
			'format' => 'html',
			'value' => 
			function($model){
				return Html::a($model->file_lpj, ['l-p-j-kegiatan/download', 'id'=> $model->id_LpjKegiatan]);
			}
		],
		['label' => 'Status Request', 'value' => $model->statusRequest->name],
	],
]) ?>

</div>
