<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LPJKegiatan */

$this->title = 'Update Data LPJ';
$this->params['breadcrumbs'][] = ['label' => 'LPJ Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lpj-kegiatan-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>