<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\ToolsColumn;
use yii\widgets\Pjax;
use yii\base\view;
use common\helpers\LinkHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KompetisiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Prestasi';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;

?>

<div class="prestasi-index">

  <?= $uiHelper->renderContentHeader($this->title);?>
  <?php

  $toolbarItemMenu =  
  "<a href='".Url::to(['create'])."' class='btn btn-success'><i class='fa fa-book'></i><span class='toolbar-label'>Upload File Prestasi</span></a>
  "
  ;

  ?>


  <?=Yii::$app->uiHelper->renderToolbar([
    'pull-left' => true,
    'groupTemplate' => ['groupStatusExpired'],
    'groups' => [
      'groupStatusExpired' => [
        'template' => ['filterStatus'],
        'buttons' => [
          'filterStatus' => $toolbarItemMenu,
        ]
      ],
    ],
  ]) ?>




<?php Pjax::begin(); ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
<<<<<<< frontend/views/prestasi/index.php
    // 'filterModel' => $searchModel,
=======
>>>>>>> frontend/views/prestasi/index.php
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'jenis_prestasi',
      'tgl_awal_kegiatan',
      'tgl_akhir_kegiatan',

      
      // 'keterangan',
    //   ['attribute' => 'upload_file',
    //   'label' => 'File Prestasi',
    //   'format' => 'html',
    //   'value' => 
    //   function($model){
    //     return Html::a($model->upload_file, ['prestasi/download', 'id'=> $model->id_prestasi]);
    //   }
    // ],
      ['attribute' => 'status_request_id',
      'label' => 'Status Request',
      'value' => function($model){
        if(is_null($model->statusRequest['name'])){
          return '-';
        }else{
          return $model->statusRequest['name'];
        }
      }
    ],

    ['class' => 'common\components\ToolsColumn',
    'template' => '{view} {edit}',
    'header' => 'Aksi',
    'buttons' => [
      'view' => function ($url, $model){
        return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
      },
      'edit' => function ($url, $model){
        if ($model->status_request_id == 1 || $model->status_request_id == 2) {
          return "";
        }else{
          return ToolsColumn::renderCustomButton($url, $model, 'Edit', 'fa fa-times');
        }
      },
    ],
    'urlCreator' => function ($action, $model, $key, $index){
      if ($action === 'view') {
        return Url::toRoute(['pdf', 'id' => $key]);
      }else if ($action === 'edit') {
        return Url::toRoute(['update', 'id' => $key]);
      }
<<<<<<< frontend/views/prestasi/index.php

    }
  ],
=======
    },
  ],
  'urlCreator' => function ($action, $model, $key, $index){
    if ($action === 'view') {
      return Url::toRoute(['pdf', 'id' => $key]);
    }else if ($action === 'edit') {
      return Url::toRoute(['update', 'id' => $key]);
    }

  }
  
],
>>>>>>> frontend/views/prestasi/index.php
],

]); ?>
<?php Pjax::end(); ?> 

</div>