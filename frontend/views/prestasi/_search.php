<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Prestasi */

?>
<div class="prestasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_prestasi') ?>

    <?= $form->field($model, 'id_mhs') ?>

    <?= $form->field($model, 'jenis_prestasi') ?>
    <?= $form->field($model, 'tgl_awal_kegiatan') ?>
    <?= $form->field($model, 'tgl_akhir_kegiatan') ?>
    <?= $form->field($model, 'upload_file') ?>
        <?= $form->field($model, 'keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
