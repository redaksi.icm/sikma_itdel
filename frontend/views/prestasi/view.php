<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Prestasi */

$this->title = 'Data Prestasi';
$this->params['breadcrumbs'][] = ['label' => ' Prestasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="prestasi-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jenis_prestasi',
            'tgl_awal_kegiatan',
            'tgl_akhir_kegiatan',
            'keterangan',
            ['attribute' => 'upload_file',
            'label' => 'File Prestasi',
            'format' => 'html',
            'value' => 
            function($model){
                return Html::a($model->upload_file, ['prestasi/download', 'id'=> $model->id_prestasi]);
            }
        ],
        ['label' => 'Status Request', 'value' => $model->statusRequest->name],
    ],
]) ?>

</div>