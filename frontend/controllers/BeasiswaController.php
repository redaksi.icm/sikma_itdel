<?php

namespace frontend\controllers;

use Yii;
use app\models\Beasiswa;
use frontend\models\BeasiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * BeasiswaController implements the CRUD actions for Beasiswa model.
 */
class BeasiswaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Beasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
      $query = Beasiswa::find();
      $searchModel = new BeasiswaSearch();
      $searchModel->id_mhs = Yii::$app->user->id;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      $Beasiswa = $query->all();
      $queryParams = Yii::$app->request->queryParams;
      if ( isset($queryParams['Beasiswa']['id_mhs']) ) {
        $queryParams['Beasiswa']['id_mhs'] = Yii::$app->user->id;
    }
    $dataProvider = $searchModel->search($queryParams);
    if (Yii::$app->user->isGuest) {
        $this->redirect(Yii::$app->urlManager->createUrl(['site/login']));
    }else{
      return $this->render('index', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'Beasiswa' => $Beasiswa,
    ]);  
  }
}
    /**
     * Displays a single Beasiswa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Beasiswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Beasiswa();

        if ($model->load(Yii::$app->request->post())) {
            $model->id_mhs = Yii::$app->user->id;

            $model->save();


            return $this->redirect(['view', 'id' => $model->id_beasiswa]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Beasiswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_beasiswa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Beasiswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Beasiswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Beasiswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Beasiswa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
